#pragma once
#include "abstract.h"

class concrete : public abstract
{
public:
    concrete();
    virtual ~concrete() override;
    virtual void printName() override;
};
