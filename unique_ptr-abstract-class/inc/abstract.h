#pragma once
class abstract
{
public:
    abstract();
    virtual void printName() = 0;
    virtual ~abstract();
};

