#include <iostream>
#include <memory>
#include "abstract.h"
#include "legacyFactory.h"
int main()
{
    std::unique_ptr<abstract> mptr(legacyFactory::createClass());
    std::cout << "Hello World!" << std::endl;
    return 0;
}
