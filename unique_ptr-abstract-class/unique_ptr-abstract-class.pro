TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        src/main.cpp \
        src/abstract.cpp \
        src/concrete.cpp \
    src/legacyFactory.cpp

HEADERS += \
        inc/abstract.h \
        inc/concrete.h \
    inc/legacyFactory.h

INCLUDEPATH += \
            inc

